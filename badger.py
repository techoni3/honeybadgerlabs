from csv import DictReader
from collections import defaultdict
import webbrowser
import matplotlib.pyplot as plt,mpld3
import datetime

class BadgerAnalysis:

    html=open("template.html").read()

    def __init__(self,csvfile):
        self.datafile=open(csvfile)
        self.reader=list(DictReader(self.datafile, skipinitialspace=True))
        self.customerCounter=defaultdict(int)
        self.distributionSchema=[1,2,3,4,5]

    def totalOrders(self):
        return len(self.reader)

    __len__= totalOrders

    def totalAmount(self):
        return sum([int(row['Amount']) for row in self.reader])

    def customerFrequency(self):
        for row in self.reader:
            self.customerCounter[row['Name']]+=1
        
    def unsatisfiedCustomers(self):
        if not self.customerCounter:
            self.customerFrequency()
        return [names for names,times in self.customerCounter.items() if times==1]
    
    def _distribute(self):
        if not self.customerCounter:
            self.customerFrequency()
        finaldict={}.fromkeys(self.distributionSchema,0)
        for count in self.customerCounter.values():
            try:
                finaldict[count]+=1
            except KeyError:
                finaldict[self.distributionSchema[-1]]+=1
        return finaldict

    def generateHTML(self):
        distribution=self._distribute()
        tabledata=""
        for orders,count in distribution.items():
            tabledata+="<tr><td>{}</td><td>{}</td></tr>".format(orders,count)
        
        fig,ax=plt.subplots()
        ax.bar(range(len(distribution)), distribution.values(), align='center')
        plt.xticks(range(len(distribution)), distribution.keys())
        self.html=self.html.format(tabledata,mpld3.fig_to_html(fig))

        with open("index.html","w+") as webpage:
            webpage.write(self.html)
        webbrowser.open("index.html")

if __name__=="__main__":

    badge=BadgerAnalysis("customerdata.txt")
    # print(badge.totalOrders())
    # OR
    # print(len(badge))
    # print(badge.totalAmount())
    # print(badge.unsatisfiedCustomers())
    badge.generateHTML()